//Letícia Munhoz
//Exercício apostila Caelum 6-9

public class Funcionario {
	

//Atributos
		public String nome;
		public String departamento;
		public double salario;
		Data dataEntrada;
		public String rg;
	
		
//Setters e Getters
		
		//nome
		public void setNome (String umNome){
	                this.nome=umNome;
	        }

	        public String getNome () {
	                return nome;
	        }

		//departamento
	        public void setDepartamento(String umDepartamento) {
	                this.departamento=umDepartamento;
	        }

	        public String getDepartamento() {
	                return departamento;
	        }

		//salario
		public void setSalario (double umSalario) {
	                this.salario=umSalario;
	        }

	        public double getSalario() {
	                return salario;
	        }

		//rg
	        public void setrg (String RG){
	                this.rg=RG;
	        }

	        public String getrg () {
	                return rg;
	        }



		public Funcionario(String nome, String departamento, double salario, String rg){
	        	this.nome=nome;
	        	this.departamento=departamento;
			this.salario=salario;
			this.rg=rg;
	        }


//Recebe Aumento		
		public void recebeAumento(int porcentagem){
			this.salario=this.salario+(this.salario*porcentagem/100);
		}


//Calculo de aumento
		public double calculaGanhoAnual (){
			return (salario*12);
		}


//Mostra
                public void mostra(){

                        System.out.println("Nome: "+this.nome);
                        System.out.println("Salario atual: "+this.salario);
                        System.out.println("Ganho anual:"+this.calculaGanhoAnual());
                        System.out.println("RG: "+this.rg);
                        System.out.println(dataEntrada.dia+"/"+dataEntrada.mes+"/"+dataEntrada.ano);

                }





}


