//Letícia Munhoz
//Exercício apostila Caelum 6-9

import java.util.*;

public class TesteFuncionario {
	
	public static void main(String[] args) {
	
		Funcionario f1 = new Funcionario (null, null, 0, null);
		Data data = new Data(0,0,0);
		

		f1.nome = "Anacleto";
		f1.departamento = "Financeiro";
		f1.salario = 5000000;
		f1.dataEntrada = data;
		f1.dataEntrada.dia = 25;
		f1.dataEntrada.mes = 11;
		f1.dataEntrada.ano = 2008;
		f1.rg = "12345678";
		f1.recebeAumento(20);
		f1.mostra();
		
	}
}
