//Letícia Munhoz
//Exercício Apostila Caelum 6-9

public class Data{

//Atributos
	public int dia;
	public int mes;
	public int ano;
	public String formatada;


//Setters e Getters
	//Dia
	public void setDia (int umDia){
		this.dia = umDia;
	}

	public int getDia(){
		return dia;
	}

	//Mes
	public void setMes (int umMes){
		this.mes = umMes;
	}
	
	public int getMes(){
		return mes;
	}

	//Ano
	public void setAno (int umAno){
		this.ano = umAno;
	}

	public int getAno(){
		return ano;
	}

//Formatada
	public void setFormatada (String umFormatada, int dia, int mes, int ano){
		this.formatada = umFormatada;
	}

//Getter e Setter Formatada
		public String getFormatada(){
			return formatada;
		}
	
//Atribuindo
	public Data (int dia, int mes, int ano){
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}


}
